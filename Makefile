PYTHON = $(shell which python3)
PIP = $(shell which pip3)
ANSIBLE = ansible-playbook
MOLECULE = molecule

V ?= 0

verbose_0 = @
verbose_2 = set -x;
verbose = $(verbose_$(V))

ansible_verbose_1 = --verbose
ansible_verbose_2 = --verbose
ansible_verbose=$(ansible_verbose_$(V))

ifneq ($(TAGS),)
	ansible_tags = "--tags=$(TAGS)"
else
	ansible_tags =
endif

all: test

env:
	@echo "PYTHON...."$(PYTHON)
	@echo "PIP......."$(PIP)
	@echo "ANSIBLE..."$(ANSIBLE)
	@echo "MOLECULE.."$(MOLECULE)
	@echo "PATH......"$(PATH)

test: install_test_deps ## Run ansible tests
	$(verbose) cd roles/common ; $(MOLECULE) test

install: install_deps ## Run installation
	echo $(verbose) $(ANSIBLE) -e 'ansible_python_interpreter=$(PYTHON)' $(ansible_verbose) setup.yml -i HOSTS --ask-become-pass --extra-vars "@config.json" $(ansible_tags)
	$(verbose) $(ANSIBLE) -e 'ansible_python_interpreter=$(PYTHON)' $(ansible_verbose) setup.yml -i HOSTS --ask-become-pass --extra-vars "@config.json" $(ansible_tags)

install_test_deps: install_deps
	$(verbose) $(PIP) install -r requirements-test.txt

install_deps: ${PYTHON} ${PIP}
	$(verbose) sudo apt install ansible
	# $(verbose) $(PIP) install -r requirements.txt

update_config_from_host: ## Update config from host
	$(verbose) cp ~/.config/nvim/bundle.vim roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/image.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/inc-rename.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/init.vim roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/lsp.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/mason.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/noice.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/nvim-cmp.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/onedarkpro.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/nvim/treesitter.config.lua roles/common/files/_config/nvim/
	$(verbose) cp ~/.config/bumblebee-status/modules/*.py roles/common/files/_config/bumblebee-status/modules/
	$(verbose) cp ~/.config/sway/* roles/common/files/_config/sway/
	$(verbose) cp ~/.config/waybar/* roles/common/files/_config/waybar/
	$(verbose) cp ~/.config/rofi/* roles/common/files/_config/rofi/
	$(verbose) cp ~/bin/kill-high-*.sh roles/common/files/bin/
	$(verbose) cp ~/bin/autoxrandr.sh roles/common/files/bin/
	$(verbose) cp ~/bin/wifixrandr.sh roles/common/files/bin/
	$(verbose) cp ~/bin/kill-high-cpu-processes.sh roles/common/files/bin/
	$(verbose) cp ~/bin/kill-high-mem-processes.sh roles/common/files/bin/
	$(verbose) cp ~/bin/kill-high-swap-processes.sh roles/common/files/bin/
	$(verbose) cp ~/bin/clean_vim.sh roles/common/files/bin/
	$(verbose) cp ~/bin/lock.sh roles/common/files/bin/
	$(verbose) cp ~/bin/power.sh roles/common/files/bin/
	$(verbose) cp ~/bin/launch.sh roles/common/files/bin/
	$(verbose) cp ~/bin/wifi-wlr-randr roles/common/files/bin/
	$(verbose) cp -r ~/.config/nvim/UltiSnips/* roles/common/files/_config/nvim/UltiSnips
	$(verbose) cp -r ~/.screenlayout/* roles/common/files/_screenlayout/
	$(verbose) cp -r ~/.cheats/* roles/common/files/_cheats/

# Help
help: ## Show this help.
	$(verbose) echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\033[33m\1\\033[m:\2/' | column -c2 -t -s :)"
