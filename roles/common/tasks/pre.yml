---
- name: Uninstall old packages
  apt:
    name: "{{ item }}"
    state: absent
  loop:
    - exuberant-ctags
  become: true
  become_method: sudo
  become_user: root

- name: Install base packages
  apt:
    name: "{{ item }}"
    state: present
    update_cache: yes
  loop:
    - aptitude
    - git
    - build-essential
    - bison
    - cmake
    - curl
    - zlib1g-dev
    - libssl-dev
    - libreadline-dev
    - libyaml-dev
    - libsqlite3-dev
    - sqlite3
    - libxml2-dev
    - libxslt1-dev
    - libcurl4-openssl-dev
    - autojump
    - tree
    - htop
    - silversearcher-ag
    - conky-all
    - fonts-font-awesome
    - direnv
    - universal-ctags
    - imagemagick
    - i3lock
    - scrot
    - jmtpfs
    - fuse
    - smem
    - alsa-utils
    - icdiff
    - python3-venv
    - xsel
    - xclip
    - screen
    - tmux
    - toilet
    - xdotool
    - pdftk
    - mc
    - fzf
    - postgresql-client
    - protobuf-compiler
    - network-manager-gnome
  become: true
  become_method: sudo
  become_user: root

- name: Check if snap does exist
  ansible.builtin.raw: which snap
  check_mode: false
  changed_when: false
  failed_when: which_res.rc > 1
  register: which_res

- name: Install base snap packages
  community.general.snap:
    name:
      - ascii-image-converter
  become: true
  become_method: sudo
  become_user: root
  when: which_res.rc == 0

- name: Create home bin dir
  file:
    path: "{{ ansible_env.HOME }}/bin"
    state: "directory"
    mode: 0750

- name: Create home tmp dir
  file:
    path: "{{ ansible_env.HOME }}/tmp"
    state: "directory"
    mode: 0755

- name: Install utilities
  copy:
    src: "roles/common/files/bin/{{ item }}"
    dest: "{{ ansible_env.HOME }}/bin/{{ item }}"
    mode: 0755
  loop:
    - git-status.sh
    - git-tag.sh
    - docker-clean.sh
    - clean_vim.sh
    - battery-level.sh
    - lock.sh
    - android.sh
    - sp
    - kill-high-cpu-processes.sh
    - kill-high-mem-processes.sh
    - kill-high-swap-processes.sh
    - brightness_control.sh
    - autoxrandr.sh
    - wifixrandr.sh
    - rename-slides.sh
    - wifi.sh
    - najma
    - power.sh
    - wifi-wlr-randr
    - launch.sh

- name: Install crontab entry for kill-high-cpu-processes.sh
  cron:
    name: "Kill high CPU processes"
    minute: "*/5"
    job: "sh {{ ansible_env.HOME }}/bin/kill-high-cpu-processes.sh"
  when: kill_processes == true

- name: Install crontab entry for kill-high-mem-processes.sh
  cron:
    name: "Kill high Mem processes"
    minute: "*/5"
    job: "sh {{ ansible_env.HOME }}/bin/kill-high-mem-processes.sh"
  when: kill_processes == true

- name: Install crontab entry for kill-high-swap-processes.sh
  cron:
    name: "Kill high Swap processes"
    minute: "*/5"
    job: "sh {{ ansible_env.HOME }}/bin/kill-high-swap-processes.sh"
  when: kill_processes == true

- name: Search GPG key for git
  shell: "gpg --list-secret-keys --with-colons {{ git_email }} 2>/dev/null | grep '^sec:' | cut -d: -f5"
  register: gpg_key_cmd
  when: git_signingkey == true

- set_fact:
    git_signingkey_value: "{{ gpg_key_cmd.stdout }}"
  when: git_signingkey == true

- name: Install .gitconfig
  template:
    src: roles/common/files/_gitconfig.j2
    dest: "{{ ansible_env.HOME }}/.gitconfig"
    mode: 0640

- name: make sure git config directory exists
  file:
    path: "{{ ansible_env.HOME }}/.config/git"
    state: directory
    mode: 0775

- name: copy git hooks
  copy:
    src: "_config/git/hooks"
    dest: "{{ ansible_env.HOME }}/.config/git/"
    mode: 0755

- name: Install git-icdiff
  get_url:
    url: "https://raw.githubusercontent.com/jeffkaufman/icdiff/master/git-icdiff"
    dest: "{{ ansible_env.HOME }}/bin/git-icdiff"
    mode: "0755"

- name: download delta
  get_url:
    url: "https://github.com/dandavison/delta/releases/download/{{ delta_version }}/git-delta_{{ delta_version }}_amd64.deb"
    dest: /tmp/delta.deb
    mode: '0644'

- name: install delta
  apt:
    deb: "/tmp/delta.deb"
    force: true
  become: true
  become_method: sudo
  become_user: root

- name: Install conky.conf
  copy:
    src: _conkyrc
    dest: "{{ ansible_env.HOME }}/.conkyrc"
    mode: 0644

- name: Install .screenrc
  copy:
    src: _screenrc
    dest: "{{ ansible_env.HOME }}/.screenrc"
    mode: 0644

- name: Install .tmux.conf
  copy:
    src: _tmux.conf
    dest: "{{ ansible_env.HOME }}/.tmux.conf"
    mode: 0644

- name: mc - skins directory
  file:
    path: "{{ ansible_env.HOME }}/.local/share/mc/skins"
    state: directory
    mode: 0750

- name: mc - install dracula theme
  get_url:
    url: "https://raw.githubusercontent.com/dracula/midnight-commander/master/skins/{{ item }}.ini"
    dest: "{{ ansible_env.HOME }}/.local/share/mc/skins/{{ item }}.ini"
    mode: "0644"
  loop:
    - dracula
    - dracula256

- name: mc - make sure mc config directory exists
  file:
    path: "{{ ansible_env.HOME }}/.config/mc"
    state: directory
    mode: 0775

- name: mc - copy ini
  copy:
    src: _config/mc/ini
    dest: "{{ ansible_env.HOME }}/.config/mc/ini"
    mode: 0644

- name: Install systemd sleep script to lock
  template:
    src: "roles/common/files/systemd/system-sleep/lock-screen.j2"
    dest: "/lib/systemd/system-sleep/lock-screen"
    mode: 0755
  become: true
  become_method: sudo
  become_user: root

- name: autoxrandr - Install config file
  copy:
    src: "roles/common/files/_config/autoxrandr/"
    dest: "{{ ansible_env.HOME }}/.config/autoxrandr"
    mode: 0644

- name: wifixrandr - Install config files
  copy:
    src: "roles/common/files/_screenlayout/"
    dest: "{{ ansible_env.HOME }}/.screenlayout"
    mode: 0644
