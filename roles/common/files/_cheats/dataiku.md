# Dataiku

#plateform/multiple #target/local #cat/UTILS

## Login to AWS
```
aws sso login
```

## Stacks diff
```
diff -uNb -x providers.tf -x deployer.tfvars terraform-deployer/aws/<StackName>/<PreVersion> terraform-deployer/aws/<StackName>/<LastVersion>
```

## Deployer terraform init
```
AWS_PROFILE=rnd terraform init
```

## Deployer terraform plan
```
AWS_PROFILE=rnd terraform plan -var-file=deployer.tfvars
```

## Deployer terraform apply
```
AWS_PROFILE=rnd terraform apply -var-file=deployer.tfvars
```

## Deployer terraform destroy
```
AWS_PROFILE=rnd terraform destroy -var-file=deployer.tfvars
```

## Deployer terraform output
```
AWS_PROFILE=rnd terraform output -json
```

## Deployer get pods (infra-dev-eu)
```
cd ~/Dev/DI/dku-infra/kubernetes-admin/clusters/infra-dev-eu
KUBECONFIG=~/.kube/config/infra-dev-eu AWS_PROFILE=rnd kubectl -n deployer-dev get pods
```

## Deployer get pods (infra-prod-eu)
```
cd ~/Dev/DI/dku-infra/kubernetes-admin/clusters/infra-prod-eu
KUBECONFIG=~/.kube/config/infra-prod-eu kubectl -n deployer get pods
```

## Deployer apply (infra-dev-eu)
```
cd ~/Dev/DI/dku-infra/kubernetes-admin/clusters/infra-dev-eu
KUBECONFIG=~/.kube/config/infra-dev-eu AWS_PROFILE=rnd kubectl apply -k . --server-side
```

## Deployer apply (infra-prod-eu)
```
cd ~/Dev/DI/dku-infra/kubernetes-admin/clusters/infra-prod-eu
KUBECONFIG=~/.kube/config/infra-prod-eu kubectl apply -k . --server-side
```
