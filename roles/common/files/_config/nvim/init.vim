let mapleader = ","
let maplocalleader = ";"

source ~/.config/nvim/bundle.vim

" Hum...
set guicursor=

" use utf8 encoding for vim files and for default file encoding
set encoding=utf-8
set fileencoding=utf-8

" auto complete on tab
set wildmenu
inoremap <expr> <CR> pumvisible() ? (complete_info().selected == -1 ? '<C-y><CR>' : '<C-y>') : '<CR>'

" tab are replaced by 2 spaces
set expandtab
set tabstop=2

" << / >> right / left shift by 3 spaces
set shiftwidth=2

" copy indent from current line on <CR>
set autoindent

" backspace in insert mode : backspace option
set backspace=eol,start,indent

" Show line number
set nu

if !exists('g:lasttab')
  let g:lasttab = 1
endif
nmap gb :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

" Change list characters and use \l to show/hide list characters
set listchars=tab:▸-,eol:$,trail:~,extends:>,precedes:<,nbsp:☠
nmap <leader>l :set list!<CR>

nmap <leader>n :tabnew<CR>
nmap <leader>w :noautocmd w<CR>

nmap <leader><up> <C-W>K
nmap <leader><down> <C-W>J
nmap <leader><left> <C-W>H
nmap <leader><right> <C-W>L
nmap <leader>$ <C-]>
nmap <leader><space> :nohlsearch<CR>
nmap <leader>t :vsplit term://zsh<CR>

" search
set hlsearch
hi Search term=underline cterm=underline ctermfg=5

" Bubble multiple lines
nmap <C-S-Up> ddkP
nmap <C-S-Down> ddp
" Bubble single lines
vmap <C-S-Up> xkP`[V`]
vmap <C-S-Down> xp`[V`]

" Page Up/Down
nmap <C-Up> <C-b>
nmap <C-Down> <C-f>

" Window
" noremap w <C-w>
" noremap W <C-w><C-w>

"  Set folding stuff
" set foldmethod=syntax
" set foldlevel=99
map <Tab> za

set clipboard=unnamedplus
set mouse-=a

" :hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
" :hi CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
:hi CursorLine   cterm=NONE ctermbg=white ctermfg=black guibg=white guifg=black
:hi CursorColumn cterm=NONE ctermbg=white ctermfg=black guibg=white guifg=black
:nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>

if filereadable(expand('.local.vimrc'))
  source .local.vimrc
endif

" For presentation -----------------------------------------------------------

let s:hidden_all = 0
function! PresentationMode()
  if s:hidden_all == 0
    let s:hidden_all = 1
    bufdo set laststatus=0
    bufdo set nonu
    bufdo set noshowmode
    bufdo set noruler
    bufdo set noshowcmd
    bufdo set signcolumn=no
    :AirlineToggle
    :SignifyDisableAll
    :IndentGuidesDisable
    call JumpFirstBuffer()
  else
    let s:hidden_all = 0
    bufdo set laststatus=2
    bufdo set nu
    bufdo set showmode
    bufdo set ruler
    bufdo set showcmd
    bufdo set signcolumn=yes
    :AirlineToggle
    :SignifyEnableAll
    :IndentGuidesEnable
  endif
  redraw
endfunction

let g:presentationBoundsDisplayed = 0
function! DisplayPresentationBoundaries()
  if g:presentationBoundsDisplayed
    match
    set colorcolumn=0
    let g:presentationBoundsDisplayed = 0
  else
    highlight lastoflines ctermbg=darkred guibg=darkred
    match lastoflines /\%23l/
    set colorcolumn=80
    let g:presentationBoundsDisplayed = 1
  endif
endfunction

function! FindExecuteCommand()
  let line = search('\S*!'.'!:.*')
  if line > 0
    let command = substitute(getline(line), "\S*!"."!:*", "", "")
    execute "silent !". command
    execute "normal gg0"
    redraw
  endif
endfunction

function! s:SaveToPDF()
  let l:hidden = 0
  if s:hidden_all == 0
    let l:hidden = 1
    call PresentationMode()
  endif
  redraw!
  sleep 100m

  call screenshot#SaveBuffersToPDF()

  if l:hidden == 1
    call PresentationMode()
  endif
endfunction

function! s:AutoSource()
    let l:testedScripts = ['syntax.vim']
    if expand('<afile>:e') !=# 'vim'
        " Don't source the edited Vimscript file itself.
        call add(l:testedScripts, 'syntax.vim')
    endif

    for l:filespec in l:testedScripts
        if filereadable(l:filespec)
            execute 'source' fnameescape(l:filespec)
        endif
    endfor

    call FindExecuteCommand()
endfunction

function! JumpFirstBuffer()
  execute "buffer 1"
endfunction

function! JumpSecondToLastBuffer()
  execute "buffer " . (len(Buffers()) - 1)
endfunction

function! JumpLastBuffer()
  execute "buffer " . len(Buffers())
endfunction

nmap <leader>b :call DisplayPresentationBoundaries()<CR>
nmap <leader>p :call PresentationMode()<CR>
noremap <S-Tab> :silent bp<CR> :redraw!<CR>
noremap <A-Tab> :silent bn<CR> :redraw!<CR>
command! -nargs=0 SaveToPDF call <SID>SaveToPDF()

" makes Ascii art font
nmap <leader>S :.!toilet -w 200 -f standard<CR>
nmap <leader>s :.!toilet -w 200 -f small<CR>
nmap <leader>F :.!toilet -w 200 -f future<CR>
" makes Ascii border
nmap <leader>1 :.!toilet -w 200 -f term -F border<CR>

" Automatically source an eponymous <file>.vim or <file>.<ext>.vim if it exists, as a bulked-up
" modeline and to provide file-specific customizations.
augroup AutoSource
    autocmd! BufNewFile,BufRead * call <SID>AutoSource()
augroup END
