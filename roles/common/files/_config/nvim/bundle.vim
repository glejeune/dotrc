
""" -*- mode: vim -*-
if &compatible
  set nocompatible
endif

filetype off

call plug#begin('~/.config/nvim/plugins')

" config
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/nerdtree'
" Plug 'projekt0n/github-nvim-theme'
" Plug 'glepnir/zephyr-nvim'
" Plug 'bluz71/vim-nightfly-colors'
Plug 'navarasu/onedark.nvim'
" Plug 'olimorris/onedarkpro.nvim'

" snipets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" search
Plug 'albfan/ag.vim'
Plug 'junegunn/fzf'

" Git
Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'

" Language
Plug 'dense-analysis/ale'

Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'RubixDev/mason-update-all'

Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'quangnguyen30192/cmp-nvim-ultisnips'
Plug 'ray-x/cmp-treesitter'
Plug 'davidsierradz/cmp-conventionalcommits'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'IndianBoy42/tree-sitter-just'

" database

Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-ui'
Plug 'kristijanhusak/vim-dadbod-completion'

" extra

Plug 'samodostal/image.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'm00qek/baleia.nvim'
Plug 'rcarriga/nvim-notify'
Plug 'folke/noice.nvim'
Plug 'MunifTanjim/nui.nvim'
Plug 'smjonas/inc-rename.nvim'

" screenshot
Plug 'https://gitlab.com/glejeune/vim-screenshot.git'
" Plug 'file://'.expand('~/Dev/GL/vim-screenshot')

" y-lang
Plug 'https://gitlab.com/y-lang/y-lang.vim.git'
" Plug 'file://'.expand('~/Dev/GL/y-lang.vim')

call plug#end()

" config ---------------------------------------------------------------------

syntax enable
set hidden

" vim-airline ----------------------------------------------------------------

let g:airline_powerline_fonts = 1
let g:airline_theme='powerlineish'
let g:airline#extensions#tabline#enabled = 1
" colorscheme github_dark
" colorscheme zephyr
" colorscheme nightfly
let g:onedark_config = {
      \   'style': 'darker',
      \   'code_style': {
      \     'comments': 'italic',
      \     'keywords': 'none',
      \     'functions': 'bold',
      \     'strings': 'none',
      \     'variables': 'none'
      \   },
      \ }
colorscheme onedark
" source ~/.config/nvim/onedarkpro.config.lua
" colorscheme onedarkpro

" nerdtree -------------------------------------------------------------------
let g:NERDTreeWinSize=45
noremap <F2> :NERDTreeToggle<CR>

" fzf ------------------------------------------------------------------------
nmap <C-P> :FZF<CR>

" ultisnips ------------------------------------------------------------------

" let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

let g:UltiSnipsEditSplit="vertical"

" signify --------------------------------------------------------------------

nmap <leader>gj <plug>(signify-next-hunk)
nmap <leader>gk <plug>(signify-prev-hunk)

" ALE ------------------------------------------------------------------------

" let g:ale_enabled = 0
let g:ale_fixers = {
\  '*': ['remove_trailing_lines', 'trim_whitespace'],
\  'javascript': ['prettier'],
\  'typescript': ['prettier'],
\  'vue': ['prettier'],
\  'go': ['goimports', 'gofmt', 'gopls'],
\  'python': ['add_blank_lines_for_python_control_statements', 'autoflake', 'autoimport', 'autopep8', 'black', 'reorder-python-imports']
\ }
let g:ale_linters = {
\  'javascript': ['eslint'],
\  'typescript': ['ts_ls', 'eslint'],
\  'vue': ['volar', 'eslint'],
\  'go': ['gofmt', 'govet', 'gopls'],
\  'python': ['flake8', 'pycodestyle', 'pylint', 'pyright']
\ }
let g:ale_linter_aliases = {'vue': ['vue', 'javascript', 'typescript']}
let g:ale_fix_on_save = 1
let g:ale_python_black_options='--line-length=79'
" let g:ale_erlang_erlc_options = '-I . -I src -I include'
" let g:ale_erlang_syntaxerl_executable = '~/bin/syntaxerl/syntaxerl'
let g:airline#extensions#ale#enabled = 1
let g:ale_sign_error = '✖︎'
let g:ale_sign_warning = '⚠'
let g:ale_echo_msg_format = '[%linter%] [%severity%] %code: %%s'
" let g:ale_set_highlights = 0
hi ALEWarningSign ctermbg=DarkYellow ctermfg=White
hi ALEError ctermbg=DarkRed cterm=none ctermfg=White
hi ALEWarning ctermbg=Black cterm=none

nmap <silent> gd <plug>(ale_go_to_definition)
nmap <silent> gv <plug>(ale_go_to_definition_in_vsplit)

" bdui ----------------------------------------------------------------------

let g:db_ui_save_location = '~/.dbui'

" custom configs ------------------------------------------------------------
source ~/.config/nvim/nvim-cmp.config.lua
source ~/.config/nvim/mason.config.lua
source ~/.config/nvim/lsp.config.lua
source ~/.config/nvim/treesitter.config.lua
source ~/.config/nvim/image.config.lua
" source ~/.config/nvim/noice.config.lua
source ~/.config/nvim/inc-rename.config.lua
