-- Setup lspconfig.
local status, nvim_lsp = pcall(require,'lspconfig')
if (not status) then return end

local protocol = require('vim.lsp.protocol')

local capabilities = require('cmp_nvim_lsp').default_capabilities(protocol.make_client_capabilities())
capabilities.textDocument.completion.completionItem.snippetSupport = true

local on_attach = function(client, buf)
  vim.api.nvim_buf_set_option(buf, "formatexpr", "v:lua.vim.lsp.formatexpr()")
	vim.api.nvim_buf_set_option(buf, "omnifunc", "v:lua.vim.lsp.omnifunc")
	vim.api.nvim_buf_set_option(buf, "tagfunc", "v:lua.vim.lsp.tagfunc")
end

-- local on_attach = function(client)
--     require'completion'.on_attach(client)
-- end

-- setup languages
-- GoLang
nvim_lsp['gopls'].setup{
  cmd = {'gopls'},
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    gopls = {
      experimentalPostfixCompletions = true,
      analyses = {
        unusedparams = true,
        shadow = true,
      },
      staticcheck = true,
    },
  },
  init_options = {
    usePlaceholders = true,
  }
}
nvim_lsp['ts_ls'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
  filetypes = { "typescript", "typescriptreact", "typescript.tsx" },
  cmd = { "typescript-language-server", "--stdio" }
}
nvim_lsp['volar'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
}
nvim_lsp['pyright'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
}
nvim_lsp['vimls'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
}
nvim_lsp['terraformls'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
}
nvim_lsp['jdtls'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
}
nvim_lsp['lua_ls'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    Lua = {
      diagnostics = {
        globals = { 'vim' },
      },
      workspace = {
        library = vim.api.nvim_get_runtime_file("", true),
        checkThirdParty = false
      },
    },
  },
}
-- nvim_lsp['rls'].setup {
--   on_attach = on_attach,
--   capabilities = capabilities,
--   settings = {
--     rust = {
--       unstable_features = true,
--       build_on_save = false,
--       all_features = true,
--     },
--   },
-- }
nvim_lsp['rust_analyzer'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    ["rust-analyzer"] = {
      imports = {
        granularity = {
          group = "module",
        },
        prefix = "self",
      },
      cargo = {
        buildScripts = {
          enable = true,
        },
      },
      procMacro = {
        enable = true
      },
    }
  }
}
