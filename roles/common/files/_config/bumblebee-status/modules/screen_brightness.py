"""Short description in RST format

   please have a look at other modules, this will go into the
   documentation verbatim (list of modules)
"""
import math
import re

import core.module
import core.widget
from util.cli import execute


class Module(core.module.Module):
    def __init__(self, config, theme):
        super().__init__(config, theme, core.widget.Widget(self.brightness))

    def brightness(self, widget):
        for line in execute("xrandr --verbose", ignore_errors=True).split("\n"):
            if "Brightness" not in line:
                continue
            match = re.search(r"\s*Brightness:\s*([\d.]+)", line)

            if match:
                brightness = math.ceil(float(match.group(1)) * 100)

                return f" {brightness}%"

        return "?"


# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
