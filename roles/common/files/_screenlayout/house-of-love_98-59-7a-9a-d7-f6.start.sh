#!/bin/sh
# xrandr    --output eDP-1      --primary  --mode 1920x1080 --pos 0x1080 --rotate normal    --output HDMI-1      --mode 3440x1440 --pos 1920x0 --rotate normal    --output DP-1      --mode 1920x1080 --pos 0x0 --rotate normal
wlr-randr --output eDP-1    --on --preferred --mode 1920x1080 --pos 0,1080 --transform normal \
          --output HDMI-A-1 --on             --mode 3440x1440 --pos 1920,0 --transform normal \
          --output DP-1     --on             --mode 1920x1080 --pos 0,0    --transform normal
