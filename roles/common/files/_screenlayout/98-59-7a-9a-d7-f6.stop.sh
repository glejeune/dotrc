#!/bin/sh
# xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output DP-2 --off --output DP-3 --off --output DP-4 --off
wlr-randr --output eDP-1    --on  --mode 1920x1080 --pos 0,0 --transform normal \
          --output DP-1     --off \
          --output HDMI-A-1 --off
