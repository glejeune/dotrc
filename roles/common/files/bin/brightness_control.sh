#!/bin/sh

CHANGE=$1
CURRENT=$(xrandr --verbose | grep -i brightness | cut -f2 -d ' ' | head -n1)
OUTPUT=$(xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1)
NEW=$(printf '%f + %f\n' "$CURRENT" "$CHANGE" | bc)
xrandr --output $OUTPUT --brightness $NEW
