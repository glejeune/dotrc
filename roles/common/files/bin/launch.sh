#!/bin/zsh

source $HOME/.profile

case "$1" in
  --*)
    selected_option="$(echo $1 | sed -e 's/^--//')"
    ;;
  *)
    exit 1
    ;;
esac

case "$selected_option" in
  window|run)
    rofi -show $selected_option -i -config ~/.config/rofi/${selected_option}.rasi
    ;;
  *)
    exit 1
    ;;
esac
