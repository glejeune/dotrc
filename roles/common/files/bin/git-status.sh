#!/bin/sh

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;32m'
PURPLE='\033[0;35m'
NC='\033[0m'

FILES=""
MFS=0
QUIET=false

normalize() {
  N=$1
  echo $N | sed -e 's/[^a-zA-Z0-9_]\+/_/g'
}

error() {
  MSG=$1
  if [ "$QUIET" = false ] ; then
    echo "$MSG" 1>&2
  fi
}

info() {
  MSG=$1
  if [ "$QUIET" = false ] ; then
    echo "$MSG"
  fi
}

usage() {
  echo "Usage: $0 [-c] [-q] [-h]" 1>&2
  echo ""
  echo "Options:"
  echo ""
  echo "  -c : Disable color"
  echo "  -q : Quiet mode"
  echo "  -h : Display this help"
  exit 1
}

while getopts "qch" OPTION ; do
  case "${OPTION}" in
    q)
      QUIET=true
      ;;
    c)
      RED=''
      GREEN=''
      YELLOW=''
      PURPLE=''
      NC=''
      ;;
    h)
      usage
  esac
done

git status 1>/dev/null 2>/dev/null
if [ ! "$?" = "0" ] ; then
  error "fatal: not a git repository (or any of the parent directories): .git"
  exit 1
fi

BRANCH_NAME=$(git symbolic-ref -q HEAD 2>/dev/null)
BRANCH_NAME=${BRANCH_NAME##refs/heads/}
info "On branch $BRANCH_NAME"

IFS='
'
for line in $(git status -s) ; do
  TYPE=$(echo $line | awk '{print $1}')
  FILE=$(echo $line | awk '{print $2}')
  FILES="$FILES $FILE"
  SIZE=$(echo $FILE | wc -c)
  if [ "$SIZE" -gt "$MFS" ] ; then
    MFS=$SIZE
  fi
  eval TYPE_$(normalize $FILE)=\$TYPE
done

for line in $(git diff --stat) ; do
  FILE=$(echo $line | awk '{print $1}')
  NUM=$(echo $line | awk '{print $3}')
  eval NUM_$(normalize $FILE)=\$NUM
  DIFF=$(echo $line | awk '{print $4}')
  eval DIFF_$(normalize $FILE)=\$DIFF
done

if [ "x$FILES" = "x" ] ; then
  info "nothing to commit, working tree clean"
  exit 0
fi

IFS=' '
for F in $FILES ; do
  TYPE=$(eval echo \$TYPE_$(normalize $F))
  NUM=$(eval echo \$NUM_$(normalize $F))
  DIFF=$(eval echo \$DIFF_$(normalize $F))
  COLOR=$NC
  case "$TYPE" in
    "M")
      COLOR=$YELLOW
      ;;
    "D")
      COLOR=$RED
      ;;
    "R")
      COLOR=$PURPLE
      ;;
    "A")
      COLOR=$GREEN
      ;;
    *)
      COLOR=$NC
  esac
  printf "% 2s ${COLOR}%-${MFS}s${NC} %-8s " $TYPE  $F $NUM
  for C in $(echo $DIFF | sed -r 's/(.)/\1 /g') ; do
    case "$C" in
      "+")
        printf "${GREEN}${C}"
        ;;
      "-")
        printf "${RED}${C}"
        ;;
      *)
        printf "${NC}${C}"
    esac
  done
  printf "$NC\n"
done
