#!/bin/sh

help() {
  __MSG=$1

  echo "Usage: $0 [--has-v | -v] major | minor | patch"
  [ -z "$__MSG" ] || echo "$__MSG"
  echo
  echo "Options:"
  echo
  echo "  --has-v -v : Force to create tag vM.m.p"
}

LAST_TAG=$(git describe --tags `git rev-list --tags --max-count=1`)
HAS_V=$(echo $LAST_TAG | grep -E "^v[0-9]+\.[0-9]+\.[0-9]+" | wc -l)
VERSION=$(echo $LAST_TAG | sed -e 's/^v//')

MAJOR=$(echo $VERSION | sed -e "s/^\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)$/\1/")
MINOR=$(echo $VERSION | sed -e "s/^\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)$/\2/")
PATCH=$(echo $VERSION | sed -e "s/^\([0-9]\+\)\.\([0-9]\+\)\.\([0-9]\+\)$/\3/")

while [ "$1" ] ; do
  case "$1" in
    major)
      if [ ! -z $CHANGED ] ; then
        help "Can't use major and $CHANGED"
      fi
      CHANGED=$1
      MAJOR=$(expr $MAJOR + 1)
      MINOR="0"
      PATCH="0"
      shift
      ;;
    minor)
      if [ ! -z $CHANGED ] ; then
        help "Can't use minor and $CHANGED"
      fi
      CHANGED=$1
      MINOR=$(expr $MINOR + 1)
      PATCH="0"
      shift
      ;;
    patch)
      if [ ! -z $CHANGED ] ; then
        help "Can't use patch and $CHANGED"
      fi
      CHANGED=$1
      PATCH=$(expr $PATCH + 1)
      shift
      ;;
    --has-v|-v)
      HAS_V="1"
      shift
      ;;
    *)
      help
      exit 0
      ;;
  esac
done

NEW_TAG="${MAJOR}.${MINOR}.${PATCH}"
[ "$HAS_V" = "1" ] && NEW_TAG="v${NEW_TAG}"

echo "Create tag $NEW_TAG"
git tag "$NEW_TAG"
