#!/bin/sh

# PROCESS="chromium-browser|WebExtensions|Web Content|slack|TabNine|flow"
PROCESS="TabNine|flow"

get_swap() {
awk '
/^SwapTotal:/ {
	swap_total=$2
}
/^SwapFree:/ {
	swap_free=$2
}
END {
	free=swap_free/1024/1024
	used=(swap_total-swap_free)/1024/1024
	total=swap_total/1024/1024

	pct=0
	if (total > 0) {
		pct=used/total*100
	}

	printf("%.f", pct)
}
' /proc/meminfo
}

export DISPLAY=:0.0
eval "export $(egrep -z DBUS_SESSION_BUS_ADDRESS /proc/$(pgrep -u $LOGNAME gdm-x-session)/environ)";

SWAP_USED=$(get_swap)

if [ $SWAP_USED -gt 80 ] ; then
  smem -c "pid swap name" -s swap -r -H | grep -E "$PROCESS" | grep -v grep | head -1 | awk '{system("notify-send -u critical -t 8000 \"New kitten saved!\" \"kill process "$3" ("$1") because '$SWAP_USED'% of swap was used!\""); system("kill -9 "$1)}'
else
  __MAX=10
  __MAX_MEM=$(free | grep "Swap" | awk '{print $2}')
  __MAX_ACC_MEM=$(($__MAX_MEM / $__MAX))
  smem -c "pid swap name" -s swap -r -H | grep -E "$PROCESS" | grep -v grep | awk '{if($2 > '$__MAX_ACC_MEM') {system("notify-send -u critical -t 8000 \"New kitten saved!\" \"kill process "$3" ("$1") using "$2" of Swap on '$__MAX_MEM'!\""); system("kill -9 "$1)}}'
fi
