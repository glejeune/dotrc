#!/bin/bash

exec > >(tee "/tmp/lock.log") 2>&1
# DSP=$(ps -u $(id -u) -o pid= | xargs -I PID -r cat /proc/PID/environ 2> /dev/null | tr '\0' '\n' | grep "^DISPLAY=:" | sort -u | awk -F= '{print $2}')
# export DISPLAY=$DSP

grim -t png /home/glejeune/tmp/screen.png
convert /home/glejeune/tmp/screen.png -scale 10% -scale 1000% /home/glejeune/tmp/screen.png
swaylock -i /home/glejeune/tmp/screen.png
rm /home/glejeune/tmp/screen.png
