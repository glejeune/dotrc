#!/bin/sh

# PROCESS="firefox.*-childID|chromium-browser.*--type=renderer|slack|TabNine|flow"
PROCESS="TabNine|flow"
MAX_SIZE=3

get_mem() {
awk '
/^MemTotal:/ {
  mem_total=$2
}
/^MemFree:/ {
  mem_free=$2
}
/^Buffers:/ {
  mem_free+=$2
}
/^Cached:/ {
  mem_free+=$2
}
END {
  free=mem_free/1024/1024
  used=(mem_total-mem_free)/1024/1024
  total=mem_total/1024/1024

  pct=0
  if (total > 0) {
    pct=used/total*100
  }

  printf("%.f", pct)
}
' /proc/meminfo
}

save_the_kitten() {
  __TYPE="$1"
  __PSV="$2"
  __MAX=$3
  __MAX_MEM=$(free | grep "$__TYPE" | awk '{print $2}')
  __MAX_ACC_MEM=$(($__MAX_MEM / $__MAX))
  ps -eo pid,$__PSV,comm,cmd --sort=-$__PSV | grep -E "$PROCESS" | grep -v grep | awk '{if($2 > '$__MAX_ACC_MEM') {system("notify-send -u critical -t 8000 \"New kitten saved!\" \"kill process "$3" ("$1") using "$2" of '$__TYPE' on '$__MAX_MEM' !\""); system("kill -9 "$1)}}'
}

export DISPLAY=:0.0
eval "export $(egrep -z DBUS_SESSION_BUS_ADDRESS /proc/$(pgrep -u $LOGNAME gdm-x-session)/environ)";

MEM_USED=$(get_mem)

if [ $MEM_USED -gt 80 ] ; then
  ps -eo pid,rss,comm,cmd --sort=-rss | grep -E "$PROCESS" | grep -v grep | head -1 | awk '{system("notify-send -u critical -t 8000 \"New kitten saved!\" \"kill process "$3" ("$1") because '$MEM_USED'% of memory was used!\""); system("kill -9 "$1)}'
else
  save_the_kitten "Mem" "rss" 3
fi
