#!/usr/bin/env bash

do_lock() {
  exec > >(tee "/tmp/lock.log") 2>&1
  grim -t png /tmp/screen.png
  convert /tmp/screen.png -scale 10% -scale 1000% /tmp/screen.png
  swaylock -i /tmp/screen.png --indicator-radius 60
  rm /tmp/screen.png
}

confirm() {
  action=$1
  need_confirm=$2

  if [ "$need_confirm" == "1" ] ; then
    config="${HOME}/.config/rofi/power-confirm.rasi"
    actions=$(echo -e "🗹 Yes\n🗷 No")
    confirm_option=$(echo -e "$actions" | rofi -dmenu -mesg "Do you really want to ${action}?" -i -config "${config}" || pkill -x rofi)
    case "$confirm_option" in
      *No)
        return
        ;;
    esac
  fi

  case "$action" in
  lock)
    do_lock
    ;;
  shutdown)
    systemctl poweroff
    ;;
  reboot)
    systemctl reboot
    ;;
  suspend)
    systemctl suspend
    do_lock
    ;;
  hibernate)
    systemctl hibernate
    ;;
  logout)
    loginctl kill-session "$XDG_SESSION_ID"
    ;;
  esac
}

case "$1" in
  --*)
    selected_option="$(echo $1 | sed -e 's/^--//')"
    ;;

  *)
    config="${HOME}/.config/rofi/power.rasi"
    actions=$(echo -e "  Lock\n  Shutdown\n  Reboot\n  Suspend\n⏲  Hibernate\n  Logout")
    selected_option=$(echo -e "$actions" | rofi -dmenu -i -config "${config}" || pkill -x rofi)
    ;;
esac

# Perform actions based on the selected option
case "$selected_option" in
*Lock|lock)
  confirm lock 0
  ;;
*Shutdown|shutdown)
  confirm shutdown 1
  ;;
*Reboot|reboot)
  confirm reboot 1
  ;;
*Suspend|suspend)
  confirm suspend 0
  ;;
*Hibernate|hibernate)
  confirm hibernate 0
  ;;
*Logout|logout)
  confirm logout 1
  ;;
esac
