#!/bin/sh

move_windows() {
  __FOCUS=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name')

  for __V in $(i3-msg -t get_workspaces | jq '.[] | select(.focused==false) | .name, .output' | awk 'NR%2{printf "%s:",$0;next;}1') ; do
    __WORKSPACE=$(echo $__V | awk -F: '{print $1}' | cut -d '"' -f 2)
    __OUTPUT=$(echo $__V | awk -F: '{print $2}' | cut -d '"' -f 2)
    __WINDOWS=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[]) | select(.window) | select(.output=="'${__OUTPUT}'") | .name' | wc -l)

    seq ${__WINDOWS} | xargs -Iz i3-msg 'workspace "'${__WORKSPACE}'"' move workspace ${__FOCUS} 1>/dev/null 2>&1
  done
  i3-msg 'workspace '${__FOCUS}'' move workspace ${__FOCUS} 1>/dev/null 2>&1
}

__NETWORK_ADAPTER=$(ip addr show $(awk 'NR==3{print $1}' /proc/net/wireless | tr -d :) | awk '/ether/{print $2}' | sed -e 's/:/-/g')
__NETWORK=$(iwgetid -r | sed -e 's/ /-/g')
__PREFIX="${__NETWORK}_${__NETWORK_ADAPTER}"
__ACTION="stop"

while [ "$1" ] ; do
  case "$1" in
    --start)
      __ACTION="start"
      shift
      ;;
    --stop)
      __ACTION="stop"
      shift
      ;;
    --info)
      echo "$HOME/.screenlayout/${__PREFIX}.start.sh"
      echo "$HOME/.screenlayout/${__PREFIX}.stop.sh"
      echo "$HOME/.screenlayout/${__NETWORK_ADAPTER}.stop.sh"
      exit 1
      ;;
  esac
done

__FILE="$HOME/.screenlayout/${__PREFIX}.${__ACTION}.sh"
[ ! -f "$__FILE" ] && __FILE="$HOME/.screenlayout/${__NETWORK_ADAPTER}.${__ACTION}.sh"
[ ! -f "$__FILE" ] && exit 1

[ "$__ACTION" = "stop" ] && move_windows

. "$__FILE"

