#!/bin/sh

# PROCESS="firefox.*-childID|chromium-browser.*--type=renderer|slack|TabNine|flow"
# PROCESS="chromium-browser.*--type=renderer|TabNine|flow"
PROCESS="TabNine|flow"

export DISPLAY=:0.0
eval "export $(egrep -z DBUS_SESSION_BUS_ADDRESS /proc/$(pgrep -u $LOGNAME gdm-x-session)/environ)";

CPU_IDLE=$(mpstat 1 1 | tail -1 | awk '{print $NF}')
CPU_USED=$(echo "100-$CPU_IDLE" | bc)

if [ $CPU_USED -gt 80 ] ; then
  ps -eo pid,%cpu,cmd --sort=-%cpu | grep -E "$PROCESS" | grep -v grep | head -1 | awk '{system("notify-send -u critical -t 8000 \"New kitten saved!\" \"kill process "$3" ("$1") because '$CPU_USED'% of CPU was used!\""); system("kill -9 "$1)}'
else
  ps -eo pid,%cpu,cmd --sort=-%cpu | grep -E "$PROCESS" | grep -v grep | awk '{if($2 > 30) {system("notify-send -u critical -t 8000 \"New kitten saved!\" \"kill process "$3" ("$1") using "$2"% of CPU!\""); system("kill -9 "$1)}}'
fi
