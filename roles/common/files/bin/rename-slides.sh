#!/bin/sh

for f in $(ls slide*) ; do
  ftmp=${f}.tmp

  echo "rename ${f} -> ${ftmp}"
  mv "${f}" "${ftmp}"
done

nb=0
for f in $(ls slide*.tmp) ; do
  nb=$((nb + 1))
  new=$(printf "slide%03d" ${nb})

  echo "rename ${f} -> ${new}"
  mv "${f}" "${new}"
done


