#!/bin/sh

TYPE="LR" # BT TB LR RL
ALIGN="C" # R L T B C
# RESOLUTION="1920x1200"
KEEP_RATIO="true"

ACTION="START"
CONFIG="$HOME/.config/autoxrandr/config.env"

while [ "$1" ] ; do
  case "$1" in
    --start)
      ACTION="START"
      shift
      ;;
    --stop)
      ACTION="STOP"
      shift
      ;;
    --type)
      shift
      TYPE=$1
      shift
      ;;
    --align)
      shift
      ALIGN=$1
      shift
      ;;
    --help)
      ACTION="HELP"
      shift
      ;;
    --config)
      shift
      CONFIG=$1
      shift
      ;;
  esac
done

[ -f "$CONFIG" ] && . "$CONFIG"

logger "Start autoxrandr TYPE=${TYPE} ALIGN=${ALIGN} ACTION=${ACTION}"

XRANDR="xrandr"
PRIMARY=$(xrandr | grep '\Wconnected' | grep primary | awk '{print $1}')
OTHERS=$(xrandr | grep '\Wconnected' | grep -v primary | awk '{print $1}' | xargs)

if [ "$TYPE" = "TB" ] || [ "$TYPE" = "LR" ] ; then
  ALL=$(echo "$PRIMARY $OTHERS")
else
  ALL=$(echo "$PRIMARY $OTHERS" | awk '{ for (i=NF; i>1; i--) printf("%s ",$i); print $1; }')
fi

MAX_WIDTH=0
for MON in $ALL ; do
  WIDTH=$(xrandr | grep -A1 "$MON" | tail -1 | awk '{print $1}' | awk -Fx '{print $1}')
  if [ "$MAX_WIDTH" -lt "$WIDTH" ] ; then
    MAX_WIDTH=$WIDTH
  fi
done

MAX_HEIGHT=0
for MON in $ALL ; do
  HEIGHT=$(xrandr | grep -A1 "$MON" | tail -1 | awk '{print $1}' | awk -Fx '{print $2}')
  if [ "$MAX_HEIGHT" -lt "$HEIGHT" ] ; then
    MAX_HEIGHT=$HEIGHT
  fi
done

get_value() {
  __TYPE=$1
  __WANTED=$(eval echo \$$(echo $__TYPE)_$(echo ${__MON} | sed 's/[^a-zA-Z0-9]/_/g'))
  if [ "x${__WANTED}" = "x" ] ; then
    echo $(eval echo \$$(echo $__TYPE))
  else
    echo $__WANTED
  fi
}

get_resolution() {
  __MON=$1

  __RESS=$(xrandr | sed -n -e "/${__MON}/,/normal/p" | tail -n +2 | head -n -1 | awk '{print $1}')
  __RESOLUTION=$(get_value "RESOLUTION")
  __KEEY_RATIO=$(get_value "KEEP_RATIO")

  __WIDTH=99999
  __HEIGHT=99999
  if ! [ "x${__RESOLUTION}" = "x" ] ; then
    __WIDTH=$(echo $__RESOLUTION | awk -Fx '{print $1}')
    __HEIGHT=$(echo $__RESOLUTION | awk -Fx '{print $2}')
  fi

  __R_WIDTH=0
  __R_HEIGHT=0

  for __RES in $(echo $__RESS) ; do
    __W=$(echo $__RES | awk -Fx '{print $1}' | sed 's/i$//g')
    __H=$(echo $__RES | awk -Fx '{print $2}' | sed 's/i$//g')
    if [ "$__W" -le "$__WIDTH" ] && [ "$__H" -le "$__HEIGHT" ] && [ "x$KEEP_RATIO" != "x" ] ; then
      if [ "$__R_WIDTH" -lt "$__W" ] || [ "$__R_HEIGHT" -lt "$__H" ] ; then
        __R_WIDTH=$__W
        __R_HEIGHT=$__H
      fi
    else 
      if [ "$__W" -le "$__WIDTH" ] || [ "$__H" -le "$__HEIGHT" ] ; then
        if [ "$__R_WIDTH" -lt "$__W" ] || [ "$__R_HEIGHT" -lt "$__H" ] ; then
          __R_WIDTH=$__W
          __R_HEIGHT=$__H
        fi
      fi
    fi
  done

  echo "${__R_WIDTH}x${__R_HEIGHT}"
}

start_multi_screen() {
  CURRENT_WIDTH=0
  CURRENT_HEIGHT=0
  XRANDR_PARAMS=""

  for MON in $ALL ; do
    # _RESOLUTION=$(xrandr | grep -A1 "$MON" | tail -1 | awk '{print $1}')
    _RESOLUTION=$(get_resolution $MON)
    WIDTH=$(echo $_RESOLUTION | awk -Fx '{print $1}')
    HEIGHT=$(echo $_RESOLUTION | awk -Fx '{print $2}')

    if [ "$TYPE" = "TB" ] || [ "$TYPE" = "BT" ] ; then
      if [ "$ALIGN" = "L" ] ; then
        X=0
      fi
      if [ "$ALIGN" = "C" ] ; then
        X=$((($MAX_WIDTH - $WIDTH) / 2))
      fi
      if [ "$ALIGN" = "R" ] ; then
        X=$(($MAX_WIDTH - $WIDTH))
      fi
      Y=$CURRENT_HEIGHT
      CURRENT_HEIGHT=$(($CURRENT_HEIGHT + $HEIGHT))
    fi

    if [ "$TYPE" = "LR" ] || [ "$TYPE" = "RL" ] ; then
      if [ "$ALIGN" = "T" ] ; then
        Y=0
      fi
      if [ "$ALIGN" = "C" ] ; then
        Y=$((($MAX_HEIGHT - $HEIGHT) / 2))
      fi
      if [ "$ALIGN" = "B" ] ; then
        Y=$(($MAX_HEIGHT - $HEIGHT))
      fi
      X=$CURRENT_WIDTH
      CURRENT_WIDTH=$(($CURRENT_WIDTH + $WIDTH))
    fi

    XRANDR_PARAMS=$(echo "${XRANDR_PARAMS} --output ${MON} --rotate normal --mode ${_RESOLUTION} --pos ${X}x${Y}")
    if [ "$MON" = "$PRIMARY" ] ; then
      XRANDR_PARAMS=$(echo "$XRANDR_PARAMS --primary")
    fi
  done
  $XRANDR $XRANDR_PARAMS
}

stop_multi_screen() {
  XRANDR_PARAMS=""
  LAST=""
  for MON in $ALL ; do
    # _RESOLUTION=$(xrandr | grep -A1 "$MON" | tail -1 | awk '{print $1}')
    _RESOLUTION=$(get_resolution $MON)
    XRANDR_PARAMS=$(echo "$XRANDR_PARAMS --output $MON")
    if [ "$MON" = "$PRIMARY" ] ; then
      XRANDR_PARAMS=$(echo "$XRANDR_PARAMS --rotate normal --mode $_RESOLUTION --primary")
    else
      XRANDR_PARAMS=$(echo "$XRANDR_PARAMS --off")
    fi
    LAST=$MON
  done
  $XRANDR $XRANDR_PARAMS
}

# TODO: check PARAMETERS

if [ "$ACTION" = "START" ] ; then
  start_multi_screen
fi

if [ "$ACTION" = "STOP" ] ; then
  stop_multi_screen
fi

if [ "$ACTION" = "HELP" ] ; then
  echo "$0 [--start|--stop] [--type LR|RL|TB|BT] [--align T|B|L|R]"
fi
