#!/bin/sh

_CONTAINER=$1

help() {
  echo "usage: docker-clean CONTAINER"
}

if [ "z" = "z${_CONTAINER}" ] ; then
  help 
  exit 1
fi

for _V in $(docker volume ls | tail -n +2 | awk '{print $2}') ; do
  _USED=$(docker ps -a --filter volume=${_V} | tail -n +2 | grep "${_CONTAINER}")
  if ! [ "z" = "z${_USED}" ] ; then
    if [ "z" = "z${_VOLUMES}" ] ; then
      _VOLUMES=$_V
    else
      _VOLUMES="$_VOLUMES $_V"
    fi
  fi
done

_IMAGE=$(docker ps -a | grep "$_CONTAINER" | awk '{print $2}')

docker rm $_CONTAINER

if ! [ "z" = "z${_IMAGE}" ] ; then
  docker rmi ${_IMAGE}
fi

for _V in ${_VOLUMES} ; do
  docker volume rm $_V
done
