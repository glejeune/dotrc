#!/bin/sh

bssid() {
  SSID=$1
  nmcli -c no -g BSSID,SSID,BARS dev wifi list | grep "$SSID" | sed -e 's/\\:/-/g' | awk -F: '{print $1}' | sed -e 's/-/:/g' | head -1
}

connect() {
  BSSID=$1
  PASSWORD=$2
  if [ "x" = "x$PASSWORD" ] ; then 
    nmcli d wifi connect $BSSID
  else
    nmcli d wifi connect $BSSID password $PASSWORD
  fi
}

help() {
  MESSAGE=$1
  echo "$0 list | scan | connect <SSID> [<password>] | help"
  if [ ! "x" = "x$MESSAGE" ] ; then
    echo $MESSAGE
  fi
}

case $1 in
  l | list)
    nmcli d wifi list --rescan no
    shift
    ;;
  s | scan)
    nmcli d wifi list --rescan yes
    shift
    ;;
  c | connect)
    ACTION=connect
    shift
    ;;
  h | help)
    help
    shift
    ;;
  *)
    help "Unknow command $1"
    shift;
esac

if [ "connect" = "$ACTION" ] ; then
  if [ "x$1" = "x" ] ; then
    help "missing network name (and password)"
  fi
  SSID=$1
  PASSWORD=$2
  BSSID=$(bssid $SSID)
  echo "Connecting to $SSID ($BSSID)"
  connect $BSSID $2
fi

