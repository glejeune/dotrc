#!/bin/sh

SWAP_PATH=~/.local/state/nvim/swap

help() {
  echo "usage: clean_vim.sh --list|-l --rm|-r"
}

case "$1" in
  --list|-l)
    find $SWAP_PATH -type f | sed -e 's/.*\///' | sed -e 's/\.swp$//' | tr '%' '/'
    ;;

  --rm|-r)
    rm -f $SWAP_PATH/*
    ;;

  *)
    help
    ;;
esac
